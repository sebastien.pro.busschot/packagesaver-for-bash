# Package Saver

The package saver save the name of installed whit the script for easy backup.

## Installation

Download this git folder in your installation directory.

```bash
git clone https://gitlab.com/sebastien.pro.busschot/packagesaver-for-bash.git
```

## Usage

```bash
# For easier usage, make an alias
# Exemple : alias install="path_installation/.installCommand.sh"
nano ~/.bashrc
source ~/.bashrc
```

### Options


```bash
# With exemple alias
install -h # show help
install -l # show all packages saved
install -b # restore all packages saved
install packages ... # install packages and save their name
```
The script generate a text file in execution folder for save the packages saved name.

## Contributing
Make all you want !

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)