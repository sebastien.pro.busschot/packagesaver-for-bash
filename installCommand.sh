#!/bin/bash

# ${0%/*} => real path of script
fileSave="${0%/*}/savedPackages"

if [ "$1" == "-b" ]
then
 #code for backup
	while read line; do
		sudo apt install -y $line
	done < $fileSave
elif [ "$1" == "-h" ]
then
	echo -e "This command can use with:\n\t-b for the backup\n\t-h for this help\n\tAll packages you want install and save for backup"
elif [ "$1" == "-l" ]
then
	echo -e "\n\tList of the packages installed: \n"
	cat $fileSave
	echo -e "\n"
else
	sa='sudo apt'
	$sa update
	for param in $@; do
		$sa install -y $param
		if [ "$?" == "0" ]
		then
			echo "$param" >> $fileSave
		fi
	done
fi
